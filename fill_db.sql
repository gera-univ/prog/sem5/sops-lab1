PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

DELETE FROM Critters;

INSERT INTO Critters VALUES('adamlem','12','Mike','Adamle','RW','2001-09-21 00:00:00','73','197','Stamford','CT');
INSERT INTO Critters VALUES('adamles','17','Scott','Adamle','D','1999-03-01 00:00:00','70','184','Columbus','OH');
INSERT INTO Critters VALUES('armanova','31','Arkady','Armanov','LW','1998-10-25 00:00:00','71','197','Minsk','RU');
INSERT INTO Critters VALUES('boolea','8','Alexi','Boole','RW','1997-09-14 00:00:00','72','194','Kiev','UK');
INSERT INTO Critters VALUES('choakd','11','Dominick','Choak','RW','1997-02-22 00:00:00','72','196','Prague','CZ');
INSERT INTO Critters VALUES('clobberk','24','Kilroy','Clobber','D','2002-06-21 00:00:00','73','200','Bangor','ME');
INSERT INTO Critters VALUES('clubbes','7','Sam','Clubbe','LW','1999-07-26 00:00:00','75','190','Paramus','NJ');
INSERT INTO Critters VALUES('finleyp','14','Peter','Finley','D','1987-06-08 00:00:00','76','194','Denver','CO');
INSERT INTO Critters VALUES('fiskj','25','Jerke','Fisk','D','2001-11-25 00:00:00','71','193','Helsinki','FI');
INSERT INTO Critters VALUES('gruberh','29','Hans','Gruber','D','1991-02-11 00:00:00','70','175','Munich','DE');
INSERT INTO Critters VALUES('grunwala','6','Allan','Grunwald','C','1990-10-17 00:00:00','74','189','Buffalo','NY');
INSERT INTO Critters VALUES('ivanovv','4','Valerei','Ivanovich','C','2004-09-20 00:00:00','72','175','Moscow','RU');
INSERT INTO Critters VALUES('jeffriea','30','Angus','Jeffries','G','1995-11-08 00:00:00','70','185','Springfield','MA');
INSERT INTO Critters VALUES('jonesr','35','Robert','Jones','C','1997-05-22 00:00:00','73','189','Hartford','CT');
INSERT INTO Critters VALUES('lexourb','9','Bruce','Lexour','D','2001-09-05 00:00:00','75','198','Quincy','IL');
INSERT INTO Critters VALUES('lunds','93','Steven','Lund','D','1997-05-22 00:00:00','71','193','St. Paul','MN');
INSERT INTO Critters VALUES('maguirea','34','Andre','Maguire','LW','1999-12-08 00:00:00','75','191','Detroit','MI');
INSERT INTO Critters VALUES('meyersd','28','Doug','Meyers','G','1998-02-11 00:00:00','70','195','Chicago','IL');
INSERT INTO Critters VALUES('olsens','37','Sandish','Olsen','D','1999-08-16 00:00:00','72','192','Stockholm','SW');
INSERT INTO Critters VALUES('quivep','20','Pierre','Quive','LW','1991-07-19 00:00:00','71','197','Quebec','QU');
INSERT INTO Critters VALUES('springej','38','Junior','Springer','C','1995-10-14 00:00:00','71','184','Toronto','ON');
INSERT INTO Critters VALUES('sullivar','39','Russel','Sullivan','G','2000-03-08 00:00:00','70','186','Vancouver','BC');
INSERT INTO Critters VALUES('travisj','19','John','Travis','C','2003-06-23 00:00:00','74','200','Boston','MA');
INSERT INTO Critters VALUES('zauberz','22','Zeke','Zauber','RW','1988-08-31 00:00:00','74','203','Moosehead','MA');

COMMIT;
