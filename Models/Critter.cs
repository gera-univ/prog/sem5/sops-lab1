﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SOPS_Lab1.Models
{
    public class Critter
    {
		[Key]
        public string PlayerId { get; set; }

        [StringLength(10)]
        [Required]
        public string Jersey { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string FirstName { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string LastName { get; set; }

        [StringLength(2)]
        [Required]
        public string Position { get; set; }

        [StringLength(60)]
        [Required]
        public string Birthday { get; set; }
        public string Weight { get; set; }
        public string Height { get; set; }
        public string BirthCity { get; set; }
        public string BirthState { get; set; }
	}
}
