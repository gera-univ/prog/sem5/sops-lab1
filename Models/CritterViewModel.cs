﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SOPS_Lab1.Models
{
    public class CritterViewModel
    {
        public List<Critter>? Critters { get; set; }
        public SelectList? Positions { get; set; }
        public SelectList? BirthdayYears { get; set; }
        public int? YearFrom { get; set; }

        [NumberNotLessThan(nameof(YearFrom))]
        public int? YearTo { get; set; }
        public string? Position { get; set; }
        public string? SearchString { get; set; }
    }
}
