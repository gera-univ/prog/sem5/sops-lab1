﻿using Microsoft.EntityFrameworkCore;

namespace SOPS_Lab1.Models
{
    public sealed class Context : DbContext
    {
        public Context(DbContextOptions<Context> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Critter> Critters { get; set; }
    }
}