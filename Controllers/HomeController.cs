﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SOPS_Lab1.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Remotion.Linq.Clauses;

namespace SOPS_Lab1.Controllers
{
    public class HomeController : Controller
    {
        private readonly Context context;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, Context context)
        {
            _logger = logger;
            this.context = context;
        }

        [Route("/")]
        public IActionResult Index(string searchString, string position, int? yearFrom, int? yearTo)
        {
            var positionQuery = from c in context.Critters orderby c.Position select c.Position;
            var birthdaysQuery = from c in context.Critters orderby c.Birthday select c.Birthday;
            var critters = from c in context.Critters select c;

            if (!string.IsNullOrEmpty(searchString))
            {
                critters = critters.Where(s => s.FirstName!.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(position))
            {
                critters = critters.Where(x => x.Position == position);
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                critters = critters.Where(s => s.FirstName!.Contains(searchString));
            }

            var dates = birthdaysQuery.Select(s => DateTime.Parse(s).Year);
            if (yearFrom != null)
            {
                critters = critters.Where(s => DateTime.Parse(s.Birthday).Year >= yearFrom);
            }
            if (yearTo != null)
            {
                critters = critters.Where(s => DateTime.Parse(s.Birthday).Year <= yearTo);
            }

            if (ModelState.IsValid)
            {
                var critterVM = new CritterViewModel()
                {
                    BirthdayYears = new SelectList(dates.Distinct()),
                    Positions = new SelectList(positionQuery.Distinct()),
                    Critters = critters.ToList()
                };

                return View(critterVM);
            }

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        private bool CritterExists(string playerId)
        {
            return context.Critters.Any(e => e.PlayerId == playerId);
        }

        public async Task<IActionResult> Edit(string? playerId)
        {
            if (playerId == null)
            {
                return NotFound();
            }

            var critter = await context.Critters.FindAsync(playerId);
            if (critter == null)
            {
                return NotFound();
            }

            return View(critter);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string playerId, [Bind("PlayerId, FirstName, LastName, Jersey, Position, Birthday")]
            Critter critter)
        {
            if (playerId != critter.PlayerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {

                try
                {
                    context.Update(critter);
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CritterExists(critter.PlayerId))
                    {
                        return NotFound();
                    }

                    throw;
                }

                return RedirectToAction(nameof(Index));
            }

            return View(critter);
        }
    }
}